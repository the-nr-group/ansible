#!/bin/bash
inotifywait -m /home/user/Downloads -e create -e moved_to |
        while read path action file; do
                if [ "${file##*.}" == "pdf" ] && [ "$action" == "MOVED_TO" ]; then
                        echo "Printing $file... "
                        lpr $path$file -P Zebra_Technologies_ZTC_ZP_450-200dpi
                fi
        done